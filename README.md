## Person Class - OOP

Define a `Person` class that gets `True` in the following tests:

```python
"""this test must not be modified"""

someone = Person()
print(someone.name == 'Robertina')
print(someone.job == 'Teacher')
print('\n')
manager = Person(job = 'Manager')
print(manager.job == 'Manager')
```


